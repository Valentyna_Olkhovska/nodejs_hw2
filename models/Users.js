const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: String,
    required: false,
  },
  password: {
    type: String,
    required: false,
  },
});

UserSchema.methods.toJSON = function() {
  const obj = this.toObject();

  delete obj.password;
  delete obj.__v;

  return obj;
};

const User = mongoose.model('User', UserSchema);

module.exports = {
  User,
};
