const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: false,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: false,
  },
});

noteSchema.methods.toJSON = function() {
  const obj = this.toObject();

  delete obj.__v;

  return obj;
};

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note,
};
