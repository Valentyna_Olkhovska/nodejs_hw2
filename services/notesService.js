const {Types} = require('mongoose');

const {Note} = require('../models/Notes.js');

const createNote = async (req, res) => {
  const {text} = req.body;
  const {userId} = req.user;

  if (!text) {
    res
        .status(400)
        .send({
          message: 'Please enter text',
        });

    return;
  }

  const note = new Note({
    userId,
    text,
    createdDate: new Types.ObjectId().getTimestamp().toISOString(),
  });

  await note.save();

  res
      .status(200)
      .send({
        message: 'Success',
      });
};

const getNotes = async (req, res) => {
  const {offset = 0, limit = 10} = req.query;
  const {userId} = req.user;

  if (offset < 0) {
    res
        .status(400)
        .send({
          message: 'Offset should be greater than or equal zero',
        });

    return;
  }

  if (limit <= 0) {
    res
        .status(400)
        .send({
          message: 'Limit should be greater than zero',
        });

    return;
  }

  if ((offset === limit) || (offset > limit)) {
    res
        .status(400)
        .send({
          message: 'Offset should be less than limit',
        });

    return;
  }

  const count = await Note.find({userId}).count();
  const notes = await Note.find({userId}).skip(offset).limit(limit);

  res
      .status(200)
      .send({
        offset,
        limit,
        count,
        notes,
      });
};

const getNote = async (req, res) => {
  const {id} = req.params;

  if (id.length !== 24) {
    res
        .status(400)
        .send({
          message: 'Not found!',
        });

    return;
  }

  const note = await Note.findById(id);

  if (!note) {
    res
        .status(400)
        .send({
          message: 'Not found!',
        });

    return;
  }

  res
      .status(200)
      .send({
        note,
      });
};

const updateNote = async (req, res) => {
  const {text} = req.body;
  const {id} = req.params;

  if (!text) {
    res.status(400)
        .send({
          message: '\'text\' should not be empty',
        });

    return;
  }

  if (id.length !== 24) {
    res
        .status(400)
        .send({
          message: 'Not found!',
        });

    return;
  }

  const note = await Note.findById(id);

  if (!note) {
    res
        .status(404)
        .send({
          message: 'Note not found',
        });

    return;
  }

  await note.updateOne({
    text,
  });

  await note.save();

  res
      .status(200)
      .send({
        message: 'Success',
      });
};

const markNoteCompleted = async (req, res) => {
  const {id} = req.params;

  if (id.length !== 24) {
    res
        .status(400)
        .send({
          message: 'Not found!',
        });

    return;
  }

  const note = await Note.findById(id);

  if (!note) {
    res
        .status(404)
        .send({
          message: 'Not found!',
        });

    return;
  };

  await note.updateOne({
    completed: !note.completed,
  });

  await note.save();

  const message = `Note has been ${note.completed ? 'unchecked' : 'checked'}!`;

  res
      .status(200)
      .send({
        message,
      });
};

const deleteNote = async (req, res) => {
  const {id} = req.params;

  if (id.length !== 24) {
    res
        .status(400)
        .send({
          message: 'Not found!',
        });

    return;
  }

  const note = await Note.findById(id);

  if (!note) {
    res
        .status(400)
        .send({
          message: 'Not found!',
        });

    return;
  };

  note.delete();

  res
      .status(200)
      .send({
        message: 'Deleted successfully!',
      });
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  deleteNote,
  markNoteCompleted,
};
