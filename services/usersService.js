const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {Types} = require('mongoose');

const {User} = require('../models/Users.js');

const MongoErrors = {
  DUPLICATE_CODE: 11000,
};

const registerUser = async (req, res) => {
  const {username, password} = req.body;

  if (!username) {
    res.status(400).send({
      message: 'Please specify \'username\' parameter',
    });
    return;
  }

  if (!password) {
    res.status(400).send({
      message: 'Please specify \'password\' parameter',
    });
    return;
  }
  const user = new User({
    username,
    createdDate: new Types.ObjectId().getTimestamp().toISOString(),
    password: await bcrypt.hash(password, 10),
  });

  try {
    await user.save();
  } catch (error) {
    if (error.code === MongoErrors.DUPLICATE_CODE) {
      res
          .status(400)
          .send({message: 'User already exist!'});

      return;
    }
    throw error;
  }

  res
      .status(200)
      .send({message: 'Success'});
};

const loginUser = async (req, res) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (!user) {
    res
        .status(400)
        .json({message: 'Username or password is invalid'});
    return;
  }

  const isPasswordCorrect = await bcrypt.compare(password, user.password);

  if (!isPasswordCorrect) {
    res
        .status(400)
        .json({message: 'Username or password is invalid'});

    return;
  }

  const payload = {username: user.username, userId: user._id};
  const jwtToken = jwt.sign(payload, process.env.JWT_KEY);

  return res
      .status(200)
      .send({
        message: 'Success',
        jwt_token: jwtToken,
      });
};

const userInfo = async (req, res) => {
  const {username} = req.user;

  const user = await User.findOne({username});

  if (!user) {
    res
        .status(404)
        .send({
          message: 'User not found',
        });

    return;
  }

  res
      .status(200)
      .send({
        user: user.toJSON(),
      });
};

const newPassword = async (req, res) => {
  const {username} = req.user;
  const {newPassword, oldPassword} = req.body;

  const user = await User.findOne({username});

  if (!user) {
    res
        .status(404)
        .send({
          message: 'User not found',
        });

    return;
  }

  const isSamePassword = oldPassword === newPassword;

  if (isSamePassword) {
    res
        .status(400)
        .send({
          message: 'New password can\'t be the same as the previous one',
        });

    return;
  }

  const isPasswordConfirmed = await bcrypt.compare(oldPassword, user.password);

  if (!isPasswordConfirmed) {
    res
        .status(400)
        .send({message: 'Password is invalid'});

    return;
  }

  await user.updateOne({
    password: await bcrypt.hash(newPassword, 10),
  });

  await user.save();

  res
      .status(200)
      .send({message: 'Success'});
};

const deleteUser = async (req, res) => {
  const {username} = req.user;

  const user = await User.findOne({username});

  if (!user) {
    res
        .status(404)
        .send({
          message: 'User not found',
        });

    return;
  }

  user.delete();

  res
      .status(200)
      .send({
        message: 'Deleted successfully!',
      });
};

module.exports = {
  registerUser,
  loginUser,
  userInfo,
  newPassword,
  deleteUser,
};
