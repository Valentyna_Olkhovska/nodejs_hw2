document.addEventListener("DOMContentLoaded", async function () {
    const JWT = localStorage.getItem('JWT');

    if (!JWT) {
        location.href = '/login.html';
        return;
    }

    const message = document.querySelector('#message');
    const saveButton = document.querySelector('#save-button');
    const signOutButton = document.querySelector('#sign-out');

    saveButton.addEventListener('click', async () => {
        try {
            await axios.post('http://localhost:8080/api/notes', {
                text: message.value
            }, {
                headers: {
                    authorization: `Bearer ${JWT}`
                }
            });
    
            location.href = '/notes.html';
        } catch (error) {
            alert(error.response.data.message);
        }
        
    });

    signOutButton.addEventListener('click', async () => {
        localStorage.removeItem('JWT');

        location.href = '/login.html';
    });
});