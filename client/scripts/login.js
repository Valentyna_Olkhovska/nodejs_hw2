document.addEventListener("DOMContentLoaded", function () {
    const submitButton = document.querySelector('#submit-button');

    submitButton.addEventListener('click', async () => {
        const username = document.querySelector('#username').value;
        const password = document.querySelector('#password').value;
        console.log(username)

        try {
            const { data } = await axios.post('http://localhost:8080/api/auth/login', {
                username,
                password
            });
            console.log(data, username)

            localStorage.setItem('JWT', data.jwt_token);
            location.href = '/notes.html';
        } catch (error) {
            alert(error.response.data.message);
        }
    });
});