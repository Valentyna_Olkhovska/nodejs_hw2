document.addEventListener("DOMContentLoaded", async function () {
    const JWT = localStorage.getItem('JWT');

    if (!JWT) {
        location.href = '/login.html';
        return;
    }

    const noteContainer = document.querySelector('#note-data');
    const signOutButton = document.querySelector('#sign-out');

    signOutButton.addEventListener('click', async () => {
        localStorage.removeItem('JWT');

        location.href = '/login.html';
    });


    const searchParams = new URLSearchParams(location.search);
    const noteId = searchParams.get('id');

    const textArea = document.createElement('textarea');

    try {
        const response = await axios.get(`http://localhost:8080/api/notes/${noteId}`, {
            headers: {
                authorization: `Bearer ${JWT}`
            }
        });

        noteContainer.innerHTML = '';

        textArea.innerText = response.data.note.text;

        noteContainer.appendChild(textArea);
    } catch (error) {
        alert(error.response.data.message);
    }

    const saveButton = document.createElement('button');
    saveButton.innerText = 'Save';

    saveButton.addEventListener('click', async () => {
        try {
            await axios.put(`http://localhost:8080/api/notes/${noteId}`, {
                text: textArea.value
            }, {
                headers: {
                    authorization: `Bearer ${JWT}`
                }
            });
            location.href = `/note.html?id=${noteId}`;
        } catch (error) {
            alert(error.response.data.message);
        }
    });

    noteContainer.appendChild(saveButton);
});