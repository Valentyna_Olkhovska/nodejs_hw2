document.addEventListener("DOMContentLoaded", async function () {
    const JWT = localStorage.getItem('JWT');

    if (!JWT) {
        location.href = '/login.html';
        return;
    }

   

    const noteContainer = document.querySelector('#note-container');
    const deleteButton = document.querySelector('.delete-button');
    const editButton = document.querySelector('.edit-button');
    const signOutButton = document.querySelector('#sign-out');

    signOutButton.addEventListener('click', async () => {
        localStorage.removeItem('JWT');

        location.href = '/login.html';
    });


    const mark = document.createElement('div');
    mark.innerHTML = 'Mark';

    mark.classList.add('mark')

    const searchParams = new URLSearchParams(location.search);
    const noteId = searchParams.get('id');
    console.log(noteId)

    try {
        const response = await axios.get(`http://localhost:8080/api/notes/${noteId}`, {
            headers: {
                authorization: `Bearer ${JWT}`
            }
        });

        if (response.data.note.completed) {
            mark.style.backgroundColor = '#06f406';
        } else {
            mark.style.backgroundColor = ''
        }


        noteContainer.innerHTML = '';

        const content = document.createElement('span');

        content.innerText = response.data.note.text;

        const container = document.createElement('div');
        container.classList.add('container')

        container.appendChild(content)
        container.appendChild(mark)

        noteContainer.appendChild(container)


        editButton.addEventListener('click', async () => {
            location.href = `/edit.html?id=${noteId}`
        })

        mark.addEventListener('click', async () => {
            try {
                const response = await axios.patch(`http://localhost:8080/api/notes/${noteId}`, {}, {
                    headers: {
                        authorization: `Bearer ${JWT}`
                    }
                });

                alert(response.data.message)

            } catch (error) {
                alert(error.response.data.message);
            }

            try {
                const response = await axios.get(`http://localhost:8080/api/notes/${noteId}`, {
                    headers: {
                        authorization: `Bearer ${JWT}`
                    }
                });
                const isCompleted = response.data.note.completed;
                if (isCompleted) {
                    mark.style.backgroundColor = '#06f406';
                    return
                }
                mark.style.backgroundColor = ''
            } catch (error) {
                alert(error.response.data.message);
            }

        })

        deleteButton.addEventListener('click', async () => {
            const confirmed = confirm('Are you sure?');

            if (!confirmed) {
                return;
            }

            try {
                await axios.delete(`http://localhost:8080/api/notes/${noteId}`, {
                    headers: {
                        authorization: `Bearer ${JWT}`
                    }
                });
                location.href = '/notes.html';
            } catch (error) {
                alert(error.response.data.message);
            }
        });


    } catch (error) {
        alert(error.response.data.message);
    }
})