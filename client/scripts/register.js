document.addEventListener("DOMContentLoaded", function () {
    const submitButton = document.querySelector('#submit-button');

    submitButton.addEventListener('click', async () => {
        const username = document.querySelector('#username').value;
        const password = document.querySelector('#password').value;

        try {
            await axios.post('http://localhost:8080/api/auth/register', {
                username,
                password
            });

            location.href = '/login.html';
        } catch (error) {
            console.log(error);
            alert(error.response.data.message);
        }
    });
});