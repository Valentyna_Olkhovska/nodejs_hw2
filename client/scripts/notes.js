document.addEventListener("DOMContentLoaded", async function () {
    const JWT = localStorage.getItem('JWT');

    if (!JWT) {
        location.href = '/login.html';
        return;
    }

    const signOutButton = document.querySelector('#sign-out');
    const notesContainer = document.querySelector('#notes-container');
    const submitButton = document.querySelector('.submit-button');

    const offset = document.querySelector('#offset-input');
    const limit = document.querySelector('#limit-input');

    signOutButton.addEventListener('click', async () => {
        localStorage.removeItem('JWT');

        location.href = '/login.html';
    });

    submitButton.addEventListener('click', async () => {

        try {
            const response = await axios.get('http://localhost:8080/api/notes', {
                headers: {
                    authorization: `Bearer ${JWT}`
                },
                params: {
                    limit: limit.value,
                    offset: offset.value
                }
            });

            notesContainer.innerHTML = '';

            response.data.notes.forEach((note) => {
                const menuButton = document.createElement('button');
                const menuLink = document.createElement('a');
                const mark = document.createElement('div');
                const wrapper = document.createElement('div');

                menuLink.setAttribute('href', `/note.html?id=${note._id}`);
                menuButton.classList.add('options-button');
                mark.classList.add('iscompleted');
                wrapper.classList.add('note-container')

                mark.innerHTML = ' ';

                if(note.completed){
                    mark.style.backgroundColor = 'green';
                    mark.innerText = '✓'
                }else{
                    mark.style.backgroundColor = ''
                    mark.innerText = ''
                }
                
                menuButton.innerHTML = 'Options'

                const text = document.createElement('span');
                text.innerText = note.text;

                wrapper.appendChild(text);
                menuLink.appendChild(menuButton);
                wrapper.appendChild(menuLink);
                wrapper.appendChild(mark)

                notesContainer.appendChild(wrapper);
            });

        } catch (error) {
            alert(error.response.data.message);
        }
    })
});