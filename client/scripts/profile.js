document.addEventListener("DOMContentLoaded", function () {
    const JWT = localStorage.getItem('JWT');

    if (!JWT) {
        location.href = '/login.html';
        return;
    }

    const changePasswordButton = document.querySelector('#change-password');
    const deleteProfileButton = document.querySelector('#delete-profile');

    changePasswordButton.addEventListener('click', async () => {
        const oldPassword = document.querySelector('#old-password').value;
        const newPassword = document.querySelector('#new-password').value;

        try {
            await axios.patch('http://localhost:8080/api/users/me', {
                oldPassword,
                newPassword
            }, {
                headers: {
                    authorization: `Bearer ${JWT}`
                },
            });

            location.href = '/notes.html';
        } catch (error) {
            alert(error.response.data.message);
        }
    });

    deleteProfileButton.addEventListener('click', async () => {
        try {
            await axios.delete('http://localhost:8080/api/users/me', {
                headers: {
                    authorization: `Bearer ${JWT}`
                },
            });

            location.href = '/login.html';
        } catch (error) {
            alert(error.response.data.message);
        }
    });
});