document.addEventListener("DOMContentLoaded", function () {
    const isAuthorized = !!localStorage.getItem('JWT');

    if (isAuthorized) {
        location.href = './notes.html'
    } else {
        location.href = './login.html'
    }
});