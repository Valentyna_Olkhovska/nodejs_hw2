const express = require('express');
const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  createNote,
  getNotes,
  getNote,
  deleteNote,
  updateNote,
  markNoteCompleted,
} = require('../services/notesService.js');
const {authMiddleware} = require('../middleware/authMiddleware');

router.post('/', authMiddleware, asyncHandler(createNote));

router.get('/', authMiddleware, asyncHandler(getNotes));

router.get('/:id', authMiddleware, asyncHandler(getNote));

router.put('/:id', authMiddleware, asyncHandler(updateNote));

router.patch('/:id', authMiddleware, asyncHandler(markNoteCompleted));

router.delete('/:id', authMiddleware, asyncHandler(deleteNote));

module.exports = {
  notesRouter: router,
};
