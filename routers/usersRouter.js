const express = require('express');
const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  userInfo,
  newPassword,
  deleteUser,
} = require('../services/usersService.js');
const {authMiddleware} = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, asyncHandler(userInfo));

router.patch('/me', authMiddleware, asyncHandler(newPassword));

router.delete('/me', authMiddleware, asyncHandler(deleteUser));

module.exports = {
  usersRouter: router,
};
