const express = require('express');
const router = new express.Router();
const asyncHandler = require('express-async-handler');

const {
  registerUser,
  loginUser,
} = require('../services/usersService.js');

router.post('/register', asyncHandler(registerUser));

router.post('/login', asyncHandler(loginUser));

module.exports = {
  authRouter: router,
};
