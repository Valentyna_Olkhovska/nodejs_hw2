const express = require('express');
const morgan = require('morgan');

require('dotenv').config();

const app = express();

const cors = require('cors');
app.use(cors());

const mongoose = require('mongoose');

const {notesRouter} = require('./routers/notesRouter.js');
const {usersRouter} = require('./routers/usersRouter.js');
const {authRouter} = require('./routers/authRouter.js');


const {
  PORT,
  USER_NAME,
  USER_PASSWORD,
  DB_NAME,
} = process.env;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

const errorHandler = (err, req, res, next) => {
  console.error(err);
  res.status(500).send({message: 'Server error'});
};

app.use(errorHandler);

const start = async () => {
  try {
    await mongoose.connect(`mongodb+srv://${USER_NAME}:${USER_PASSWORD}@cluster0.zg9u1gp.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`);
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
